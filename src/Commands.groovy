class Commands {

    static def commands = [:]

    static def handleCommand(Clone clone, String cmd, String hostmask, String[] params, String data) {
        if (!commands.containsKey(cmd)) return
        commands[cmd].call(clone, hostmask, params, data)
    }

    static def addCommand(String name, Closure callback) {
        commands[name] = callback
    }
}