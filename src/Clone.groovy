class Clone {

    def name, master, plan, host, port, proxy, reader, writer, channel
    def Socket sock

    def Clone(String name, boolean master, InvasionPlan plan, String channel) {
        this.name = name
        this.master = master
        this.plan = plan
        this.channel = channel
    }

    def connect(String host, int port, Proxy proxy) {
        this.host = host
        this.port = port
        this.proxy = proxy

        sock = new Socket(host, port)
        reader = new BufferedReader(new InputStreamReader(sock.getInputStream()))
        writer = new BufferedWriter(new OutputStreamWriter(sock.getOutputStream()))

        sendRaw("NICK ${name}")
        sendRaw("USER ${name} ${host} 0 :${name}")

        while(true) {
            def line = reader.readLine()
            if (line == null) throw new Exception('Connection closed')
            if (line.startsWith('PING')) {
                sendRaw("PONG${line.substring(4)}")
                continue
            }
            def r = line.split(' ')[1]
            if (r == '001') break;
            if (r == '433') throw new Exception('Nick in use')

        }

        listen()
    }

    def listen() {
        Thread.start {
            while (true) {
                def line = reader.readLine()
                if (line == null) throw new Exception('Connection closed')
                println "[$name] >>> $line"
                if (line.startsWith('PING')) {
                    sendRaw("PONG${line.substring(4)}")
                    continue
                }
                String[] parts = line.split(' ')
                if (parts.length < 2) continue
                String hostmask = parts[0];
                String cmd = parts[1]
                String rest = line.substring(hostmask.length() + cmd.length() + 2)
                parts = rest.split(' :')
                String params_ = parts[0]
                String[] params = params_.split(' ')
                String data
                try {
                    data = rest.substring(params_.length() + 2)
                } catch (Exception ex) {
                    data = ''
                }

                Commands.handleCommand(this, cmd, hostmask, params, data)
            }
        }
    }

    def sendRaw(String line) {
        writer.write(line + "\r\n")
        println "[$name] <<< $line"
        writer.flush()
    }

}
