class InvasionPlan {

    def nicklist = [];
    def String channel, host
    def int count, port, nickcnt = 0
    def Clone master
    def clones = []

    def InvasionPlan(String host, int port, String channel, nicklist, int count) {
        this.host = host
        this.channel = channel
        this.nicklist = nicklist
        this.count = count
        this.port = port
        this.host = host
    }

    def broadcast(String message) {
        clones*.sendRaw(message)
    }

    String makeNick() {
        nicklist[nickcnt++]
    }

    def connectClone(boolean master) {
        //def Clone(String name, boolean master, InvasionPlan plan, String channel) {
        def c = new Clone(makeNick(), master, this, channel)
        c.connect(host, port, null)
        c
    }

    def prepare() {
        master = connectClone(true)
        master.sendRaw("JOIN ${channel}")
    }

    def execute() {
        for (int i = 0; i < count - 1; i++) {
            clones << connectClone(false)
        }
    }

    def shutdown() {
        broadcast("QUIT :BAI")
    }

}
