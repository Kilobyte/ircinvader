class Main {
    def main(String... args) {
        Commands.addCommand('PRIVMSG') {Clone clone, hostmask, params, data ->
            String tar = params[0]
            if (tar.startsWith('#')) {
                // channel
                if (clone.master && tar == clone.channel) {
                    clone.plan.broadcast data
                }
            } else {
                if (clone.master) {
                    clone.plan.broadcast data
                    clone.sendRaw "PRIVMSG ${clone.channel} :$data"
                }
            }
        }

        def r = new BufferedReader(new FileReader(new File('nicks.txt')))
        def nicklist = []
        while (true) {
            def n = r.readLine()
            if (n == null) break
            nicklist << n
        }

        def server = 'irc.pc-logix.com'
        def port = 6667
        def count = 6
        def channel = '#kilonet-ctrl'

        def plan = new InvasionPlan(server, port, channel, nicklist, count)
        plan.prepare()
        plan.execute()

        plan.broadcast('JOIN #stary')

        System.addShutdownHook {
            plan.shutdown()
        }
    }
}